import {
  FETCH_RESTAURANTS_FAILURE,
  FETCH_RESTAURANTS_REQUEST,
  FETCH_RESTAURANTS_SUCCESS,
  FETCH_EXACT_RESTAURANT_REQUEST,
  FETCH_EXACT_RESTAURANT_SUCCESS,
  FETCH_EXACT_RESTAURANT_FAILURE, ADD_REVIEW_SUCCESS,
} from "../actions/restaurantsActions";

const initialState = {
  restaurants: [],
  fetchingLoading: false,

  restaurant: null,
  fetchingExactRestaurant: false,
};

const restaurantsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_RESTAURANTS_REQUEST:
      return {...state, fetchingLoading: true};
    case FETCH_RESTAURANTS_SUCCESS:
      return {...state, fetchingLoading: false, restaurants: action.payload};
    case FETCH_RESTAURANTS_FAILURE:
      return {...state, fetchingLoading: false};
    case FETCH_EXACT_RESTAURANT_REQUEST:
      return {...state, fetchingExactRestaurant: true};
    case FETCH_EXACT_RESTAURANT_SUCCESS:
      return {...state, fetchingExactRestaurant: false, restaurant: action.payload[0]};
    case FETCH_EXACT_RESTAURANT_FAILURE:
      return {...state, fetchingExactRestaurant: false}
    case ADD_REVIEW_SUCCESS:
        return {...state, restaurant: action.payload};
    default:
      return state;
  }
};

export default restaurantsReducer;

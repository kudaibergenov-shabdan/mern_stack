// react, react-dom, react-router-dom
import React from 'react';
import ReactDOM from 'react-dom';
import {Router} from "react-router-dom";

// redux, react-redux, redux-thunk
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {Provider} from "react-redux";
import thunk from "redux-thunk";

//other imports
import {MuiThemeProvider} from "@material-ui/core";
import theme from "./theme";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

// import reducers and files
import usersReducer, {initialState} from "./store/reducers/usersReducer";
import restaurantsReducer from "./store/reducers/restaurantsReducer";
import history from "./history";
import axiosApi from "./axiosApi";
import App from './App';
import {loadFromLocalStorage, saveToLocalStorage} from "./store/localStorage";

const rootReducer = combineReducers({
    'users': usersReducer,
    'restaurants': restaurantsReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk))
);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            ...initialState,
            user: store.getState().users.user,
        }
    });
});

axiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token
    } catch (e) {}

    return config;
});

const app = (
    <Provider store={store}>
        <Router history={history}>
            <MuiThemeProvider theme={theme}>
                <ToastContainer/>
                <App/>
            </MuiThemeProvider>
        </Router>
    </Provider>
)

ReactDOM.render(app, document.getElementById('root'));

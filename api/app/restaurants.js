const express = require('express');
const Restaurant = require('../models/Restaurant');
const multer = require("multer");
const {nanoid} = require("nanoid");

const authForUser = require('../middleware/authForUser');
const permit = require('../middleware/permit');
const path = require("path");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

// Get request by id of restaurant available for all users.
router.get('/', async (req, res) => {
    try {
        const query = {
            _id: req.query.id,
        };
        if (!query._id) {
            delete query._id;
        }

        const restaurant = await Restaurant.find(query);
        if (restaurant.length === 0) {
            if (query._id) {
                return res.status(404).send({error: 'Such restaurant does not exist'});
            } else {
                return res.status(404).send({error: 'No restaurants found'});
            }
        }
        res.send(restaurant);

    } catch (e) {
        res.status(500).send(e);
    }
});

// Post-request to create new restaurant available only for registered user.
router.post('/new', authForUser, permit('user'), upload.single('displayImage'), async (req, res) => {
    try {
        const restaurantData = {
            name: req.body.name,
            user: req.user,
            description: req.body.description || null,
            agreeTerms: JSON.parse(req.body.agreeTerms)
        };

        if (restaurantData.agreeTerms && typeof restaurantData.agreeTerms == 'boolean') {

            if (req.file) {
                restaurantData.displayImage = 'uploads/' + req.file.filename;
            }

            const restaurant = new Restaurant(restaurantData);
            await restaurant.save();
            res.send(restaurant);
        } else {
            res.status(404).send({error: 'You have not accepted terms'});
        }

    } catch (e) {
        res.status(500).send(e);
    }
});

router.put('/:id', authForUser, permit('user'), async (req, res) => {
    try {
        const restaurant = await Restaurant.findById(req.params.id);

        if (!restaurant) {
            return res.status(404).send({error: 'You are trying to modify non-existent restaurant'});
        } else if (restaurant.user.equals(req.user._id)) {
            return res.status(404).send({error: 'It is impossible to leave comment to owner'});
        }

        const query = {
            user: req.user,
            review: req.body.review,
            rateQualityOfFood: req.body.rateQualityOfFood,
            rateServiceQuality: req.body.rateServiceQuality,
            rateInterior: req.body.rateInterior,
        };

        if (!query.review || !query.rateQualityOfFood || !query.rateServiceQuality || !query.rateInterior) {
            return res.status(400).send({error: 'Please, fill in all fields'});
        }

        const updatedRestaurant = await Restaurant.findByIdAndUpdate(req.params.id,
            {
                $push: {
                    feedBack: query
                }
            }, {
                new: true
            },
        );

        res.send(updatedRestaurant);
    } catch (e) {
        res.status(500).send(e);
    }
})

module.exports = router;
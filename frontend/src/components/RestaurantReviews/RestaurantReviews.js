import React from 'react';
import {Grid, Typography} from "@material-ui/core";

const RestaurantReviews = ({restaurant}) => {
  return (
    <>
      {
        restaurant.feedBack.length > 0
          ?
          <>
            <Typography><b>Reviews:</b></Typography>
            <Grid container>
              {
                restaurant.feedBack.map(item => (
                  <Grid key={item._id} item xs={12}>
                    Date {item.dateCreated}: {item.review}
                    <Typography>Quality of food: {item.rateQualityOfFood}</Typography>
                    <Typography>Service Quality: {item.rateServiceQuality}</Typography>
                    <Typography>Interior: {item.rateInterior}</Typography>
                  </Grid>
                ))
              }
            </Grid>
          </>
          :
          <Typography>No reviews</Typography>
      }
    </>
  );
};

export default RestaurantReviews;
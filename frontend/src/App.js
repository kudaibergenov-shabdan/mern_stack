import {Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Login from "./containers/Login/Login";
import Main from "./containers/Main/Main";
import Register from "./containers/Register/Register";
import ExactRestaurant from "./containers/ExactRestauran/ExactRestaurant";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Main}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
            <Route path="/restaurants" component={ExactRestaurant}/>
        </Switch>
    </Layout>
);

export default App;

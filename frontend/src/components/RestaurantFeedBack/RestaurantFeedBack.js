import React from 'react';
import {Grid, Typography} from "@material-ui/core";

const RestaurantFeedBack = ({restaurant}) => {
  return (
    <>
      {
        restaurant.feedBack.length > 0
          ?
          <Grid item xs={12}>
            <Typography><b>Ratings:</b></Typography>
            <Typography>Quality of food:
              {(restaurant.feedBack.reduce((acc, item) => {
                  return acc + item.rateQualityOfFood
                }, 0
              ) / restaurant.feedBack.length).toFixed(1)}
            </Typography>
            <Typography>Service quality:
              {(restaurant.feedBack.reduce((acc, item) => {
                  return acc + item.rateServiceQuality
                }, 0
              ) / restaurant.feedBack.length).toFixed(1)}
            </Typography>
            <Typography>Service quality:
              {(restaurant.feedBack.reduce((acc, item) => {
                  return acc + item.rateInterior
                }, 0
              ) / restaurant.feedBack.length).toFixed(1)}
            </Typography>
          </Grid>

          :
          <Typography>No rating info</Typography>
      }
    </>
  );
};

export default RestaurantFeedBack;
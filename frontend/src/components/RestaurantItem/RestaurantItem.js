import React from 'react';
import {Button, Card, CardActions, CardContent, CardMedia, Grid, makeStyles, Typography} from "@material-ui/core";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import {useDispatch} from "react-redux";
import {historyPush} from "../../store/actions/historyActions";

const useStyles = makeStyles(theme => ({
    card: {
        height: '100%',
    },
    media: {
        textAlign: 'center',
    },
    mediaImage: {
        maxWidth: '200px',
        minHeight: '150px',
        height: 'auto',
    },
}));

const RestaurantItem = ({id, name, image, feedBack, galleryImages}) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    let cardImage = imageNotAvailable;
    if (image) {
        const pathToImage = apiURL + '/';
        cardImage = pathToImage + image;
    }

    const visitRestaurant = id => {
        dispatch(historyPush(`/restaurants?id=${id}`));
    };

    return (
        <Grid container item xs={3}>
            <Card className={classes.card}>
                <CardMedia className={classes.media}>
                    <img src={cardImage} alt={name} className={classes.mediaImage}/>
                </CardMedia>
                <CardContent>
                    <Typography variant="body1" color="textPrimary">
                        {name}
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                        {feedBack.length}&nbsp;reviews
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                        Avg:&nbsp;
                        {feedBack.length > 0
                            ?
                            (
                                feedBack.reduce((acc, feedBackItem) => {
                                    return acc
                                        + feedBackItem.rateQualityOfFood
                                        + feedBackItem.rateServiceQuality
                                        + feedBackItem.rateInterior
                                }, 0) / feedBack.length / 3
                            ).toFixed(1)
                            : 'no info'
                        }
                    </Typography>
                    <Typography variant="body2" color="textSecondary">
                        {galleryImages.length}&nbsp;photos
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button
                        variant="contained"
                        color="secondary"
                        onClick={() => visitRestaurant(id)}
                    >
                        Visit
                    </Button>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default RestaurantItem;
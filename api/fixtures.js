const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Restaurant = require("./models/Restaurant");

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    await User.create({
        email: 'admin@gmail.com',
        password: '123',
        token: nanoid(),
        role: 'admin',
        displayName: 'Admin',
    });

    const [user1, user2, user3, user4] = await User.create(
        {
            email: 'user1@gmail.com',
            password: '123',
            token: nanoid(),
            role: 'user',
            displayName: 'User1',
        },
        {
            email: 'user2@gmail.com',
            password: '123',
            token: nanoid(),
            role: 'user',
            displayName: 'User2',
        },
        {
            email: 'user3@gmail.com',
            password: '123',
            token: nanoid(),
            role: 'user',
            displayName: 'User3',
        },
        {
            email: 'user4@gmail.com',
            password: '123',
            token: nanoid(),
            role: 'user',
            displayName: 'User4',
        },
    );

    await Restaurant.create(
        {
            name: 'Аристократ',
            user: user1,
            displayImage: 'fixtures/ballroom.jpg',
            description: 'Here is a wonderful place to have a rest and write some more REST API!',
            feedBack: [
                {
                    user: user2,
                    rateQualityOfFood: 5,
                    rateServiceQuality: 5,
                    rateInterior: 5,
                    review: 'I like it so much... Waiting for the moment to come again',
                },
                {
                    user: user3,
                    rateQualityOfFood: 4,
                    rateServiceQuality: 4,
                    rateInterior: 4,
                    review: 'Not bad. But not perfect !',
                },
            ]
        },
        {
            name: 'Брынза',
            user: user2,
            displayImage: 'fixtures/brynza.jpeg',
            description: 'Here is a wonderful place to have a rest and write some more REST API!'
        },
        {
            name: 'Eat',
            user: user3,
            displayImage: 'fixtures/eat.jpg',
            description: 'Here is a wonderful place to have a rest and write some more REST API!'
        },
    );

    await mongoose.connection.close();
};

run().catch(console.error);
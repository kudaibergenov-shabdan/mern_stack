const path = require('path');

const rootPath = __dirname;

let dbUrl = 'mongodb://localhost/mern_app';
let port = 8000;

module.exports = {
    rootPath,
    port,
    uploadPath: path.join(rootPath, 'public/uploads'),
    db: {
        url: dbUrl,
    },
    facebook: {
        appId: process.env.FACEBOOK_APP_ID,
        appSecret: process.env.FACEBOOK_APP_SECRET,
    },
};
require('dotenv').config();
const express = require('express');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const mongoose = require("mongoose");
const config = require('./config');
const users = require('./app/users');
const restaurants = require('./app/restaurants');


const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/users', users);
app.use('/restaurants', restaurants);

const run = async () => {
    await mongoose.connect(config.db.url);

    app.listen(config.port, () => {
        console.log(`Server started on ${config.port} port!`);
    });

    exitHook(() => {
       console.log('exiting');
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e));
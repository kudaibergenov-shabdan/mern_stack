const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const validateUnique = async value => {
    const restaurant = await Restaurant.findOne({
        name: value,
    });
    if (restaurant) return false;
}

const FeedbackSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
    },
    dateCreated: {
        type: Date,
        default: new Date(),
    },
    rateQualityOfFood: {
        type: Number,
    },
    rateServiceQuality: Number,
    rateInterior: Number,
    review: String
});

const RestaurantSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        validate: [{
            validator: validateUnique,
            message: 'This restaurant is already existed',
        },]
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    displayImage: String,
    galleryImages: [String],
    feedBack: [FeedbackSchema],
    description: String,
});

RestaurantSchema.plugin(idValidator);
const Restaurant = mongoose.model('Restaurant', RestaurantSchema);

module.exports = Restaurant;
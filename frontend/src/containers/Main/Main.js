import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {fetchRestaurants} from "../../store/actions/restaurantsActions";
import {Button, CircularProgress, Grid} from "@material-ui/core";
import RestaurantItem from "../../components/RestaurantItem/RestaurantItem";

const Main = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const restaurants = useSelector(state => state.restaurants.restaurants);
    const fetchingLoading = useSelector(state => state.restaurants.fetchingLoading);

    useEffect(() => {
        dispatch(fetchRestaurants());
    },[dispatch]);

    return (
        // <div>
        //     Main container
        // </div>
        <Grid container direction="column" spacing={2}>
            <Grid item container justifyContent="space-between" alignItems="center">
                {user && (
                    <Grid item>
                        <Button
                            color="primary"
                            variant="contained"
                            component={Link}
                            to="/restaurant"
                        >Add place
                        </Button>
                    </Grid>
                )}
            </Grid>
            <Grid item>
                <Grid item container justifyContent="center" direction="row" spacing={1}>
                    {fetchingLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : (
                        restaurants.map(item => (
                            <RestaurantItem
                                key={item._id}
                                id={item._id}
                                name={item.name}
                                image={item.displayImage}
                                feedBack={item.feedBack}
                                galleryImages={item.galleryImages}
                            />
                        ))
                    )}
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Main;
import React, {useEffect} from 'react';
import {useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchRestaurant} from "../../store/actions/restaurantsActions";
import {CircularProgress, Grid, Typography} from "@material-ui/core";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import RestaurantFeedBack from "../../components/RestaurantFeedBack/RestaurantFeedBack";
import RestaurantReviews from "../../components/RestaurantReviews/RestaurantReviews";
import AddReview from "../../components/AddReview/AddReview";

const ExactRestaurant = () => {
  const location = useLocation();
  const dispatch = useDispatch();
  const restaurant = useSelector(state => state.restaurants.restaurant);
  const fetchingExactRestaurant = useSelector(state => state.restaurants.fetchingExactRestaurant);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    const urlParams = new URLSearchParams(location.search);
    const restaurantId = urlParams.get('id');
    dispatch(fetchRestaurant(restaurantId));
  }, [dispatch]);

  let cardImage = imageNotAvailable;

  return (
    <>
      {
        fetchingExactRestaurant
          ?
          (
            <Grid container justifyContent="center" alignItems="center">
              <Grid item>
                <CircularProgress/>
              </Grid>
            </Grid>
          )
          :
          (restaurant &&
            (
                <Grid container spacing={3}>
                  <Grid item xs={12} md={6}>
                    <Typography><b>Restaurant name:</b> {restaurant.name}</Typography>
                    <Typography><b>Description:</b> {restaurant.description}</Typography>
                    <RestaurantFeedBack restaurant={restaurant}/>
                    <RestaurantReviews restaurant={restaurant}/>
                  </Grid>
                  <Grid item xs={12} md={6}>
                      <div>
                          <img src={restaurant.displayImage
                              ? apiURL + '/' + restaurant.displayImage
                              : cardImage}
                               alt={restaurant.name}
                               width="80%"
                               height="auto"
                          />
                      </div>
                  </Grid>
                  <Grid item xs={12} md={12}>
                    <AddReview user={user} restaurantID={restaurant._id} />
                  </Grid>
                </Grid>
            )
          )
      }
    </>
  );
};

export default ExactRestaurant;
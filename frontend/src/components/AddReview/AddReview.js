import React, {useState} from 'react';
import {Button, Grid, MenuItem, TextField} from "@material-ui/core";
import {useDispatch} from "react-redux";
import {addReview} from "../../store/actions/restaurantsActions";

const marks = ['1', '2', '3', '4', '5'];

const AddReview = ({user, restaurantID}) => {
  const dispatch = useDispatch();

  const [reviewData, setReviewData] = useState({
    review: '',
    rateQualityOfFood: '',
    rateServiceQuality: '',
    rateInterior: '',
  });

  const onChangeHandler = e => {
    const {name, value} = e.target;
    setReviewData(prev => ({
      ...prev,
      [name]: value,
    }));
  };

  const submitForm = () => {
      dispatch(addReview(restaurantID, reviewData));
  };

  return (
    <>
      {
        user &&
        (
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12}>
              <TextField
                required
                label="Add review"
                multiline
                rows={4}
                name="review"
                value={reviewData.review}
                onChange={e => onChangeHandler(e)}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                required
                select
                label="Quality of food"
                name="rateQualityOfFood"
                value={reviewData.rateQualityOfFood}
                onChange={e => onChangeHandler(e)}
              >
                {
                  marks.map(mark => (
                    <MenuItem key={mark} value={mark}>
                      {mark}
                    </MenuItem>
                  ))
                }
              </TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                required
                select
                label="Service quality"
                name="rateServiceQuality"
                value={reviewData.rateServiceQuality}
                onChange={e => onChangeHandler(e)}
              >
                {
                  marks.map(mark => (
                    <MenuItem key={mark} value={mark}>
                      {mark}
                    </MenuItem>
                  ))
                }
              </TextField>
            </Grid>
            <Grid item xs={3}>
              <TextField
                required
                select
                label="Interior"
                name="rateInterior"
                value={reviewData.rateInterior}
                onChange={e => onChangeHandler(e)}
              >
                {
                  marks.map(mark => (
                    <MenuItem key={mark} value={mark}>
                      {mark}
                    </MenuItem>
                  ))
                }
              </TextField>
            </Grid>
            <Grid item xs={3}>
              <Button
                type="button"
                fullWidth
                variant="contained"
                color="primary"
                onClick={submitForm}
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        )
      }
    </>
  )
}
export default AddReview;
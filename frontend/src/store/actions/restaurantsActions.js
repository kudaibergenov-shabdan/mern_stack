import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';

export const FETCH_RESTAURANTS_REQUEST = 'FETCH_RESTAURANTS_REQUEST';
export const FETCH_RESTAURANTS_SUCCESS = 'FETCH_RESTAURANTS_SUCCESS';
export const FETCH_RESTAURANTS_FAILURE = 'FETCH_RESTAURANTS_FAILURE';

export const FETCH_EXACT_RESTAURANT_REQUEST = 'FETCH_EXACT_RESTAURANT_REQUEST';
export const FETCH_EXACT_RESTAURANT_SUCCESS = 'FETCH_EXACT_RESTAURANT_SUCCESS';
export const FETCH_EXACT_RESTAURANT_FAILURE = 'FETCH_EXACT_RESTAURANT_FAILURE';

export const ADD_REVIEW_REQUEST = 'ADD_REVIEW_REQUEST';
export const ADD_REVIEW_SUCCESS = 'ADD_REVIEW_SUCCESS';
export const ADD_REVIEW_FAILURE = 'ADD_REVIEW_FAILURE';


export const fetchRequestRestaurants = () => ({type: FETCH_RESTAURANTS_REQUEST});
export const fetchSuccessRestaurants = restaurants => ({type: FETCH_RESTAURANTS_SUCCESS, payload: restaurants});
export const fetchFailureRestaurants = error => ({type: FETCH_RESTAURANTS_FAILURE, payload: error});

export const fetchRequestExactRestaurant = () => ({type: FETCH_EXACT_RESTAURANT_REQUEST});
export const fetchSuccessExactRestaurant = exactRestaurant => ({
  type: FETCH_EXACT_RESTAURANT_SUCCESS,
  payload: exactRestaurant
});
export const fetchFailureExactRestaurants = error => ({type: FETCH_EXACT_RESTAURANT_FAILURE, payload: error});

export const addReviewRequest = () => ({type: ADD_REVIEW_REQUEST});
export const addReviewSuccess = review => ({type: ADD_REVIEW_SUCCESS, payload: review});
export const addReviewFailure = error => ({type: ADD_REVIEW_FAILURE, payload: error});

export const fetchRestaurants = () => {
  return async dispatch => {
    try {
      dispatch(fetchRequestRestaurants());
      const response = await axiosApi.get('/restaurants');
      dispatch(fetchSuccessRestaurants(response.data));
    } catch (error) {
      dispatch(fetchFailureRestaurants(error));
      toast.error('Could not fetch restaurants', {
        position: toast.POSITION.BOTTOM_RIGHT,
        theme: 'colored',
        icon: <WarningIcon/>
      })
    }
  };
};

export const fetchRestaurant = restaurantId => {
  return async dispatch => {
    try {
      dispatch(fetchRequestExactRestaurant());
      const response = await axiosApi.get(`/restaurants?id=${restaurantId}`);
      dispatch(fetchSuccessExactRestaurant(response.data));
    } catch (error) {
      dispatch(fetchFailureExactRestaurants(error));
      toast.error('Could not fetch selected restaurant', {
        position: toast.POSITION.BOTTOM_RIGHT,
        theme: 'colored',
        icon: <WarningIcon/>
      })
    }
  };
};

export const addReview = (restaurantID, reviewData) => {
  return async dispatch => {
    try {
      dispatch(addReviewRequest());
      const response = await axiosApi.put(`/restaurants/${restaurantID}`, reviewData);
      dispatch(addReviewSuccess(response.data));
      toast.success('Review has been successfully added', {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
    }
    catch (error) {
      dispatch(addReviewFailure(error));
      toast.error(error.response.data.error, {
        position: toast.POSITION.BOTTOM_RIGHT,
      });
      throw new Error();
    }
  };
};